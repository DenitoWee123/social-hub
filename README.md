# Social Hub
Проектна документация 

 

Проект SocialHUB цели да създаде лесно достъпна онлайн платформа, чрез която физически лица, организации и местни производители лесно да комуникират помежду си. Именно за това е важно уебсайтът да бъде информативен, лесно достъпен и със семпъл, но атрактивен дизайн. 

 

1. Цел 

Целта на създаване на сайт на SocialHUB е да се улесни достъпа до услугите на програмата за потребителите и производителите и да осъществи комуникация между тях. Сайтът трябва да е лесен и удобен за използване от всякакви хора. 

2. Целеви групи 

Уебсайтът на SocialHUB ще бъде насочен към следните целеви групи: 

Малкия и среден бизнес;  

Потенциални инвеститори; 

Клиенти; 

3. Време за изпълнение 

Времето за изпълнение се очаква да е между 2 до 4 седмици. 

4. Очакван резултат 

Очаква се създаването на нов, лесно достъпен и безопасен уебсайт, който да изпълни целите на програмата SocialHUB. 

5. Специфични изисквания към сайта 

Уебсайтът на SocialHUB трябва да изпълнява на следните задачи: 

- Да информира потребители на SocialHUB за целта и услугите на сайта; 

- Да е достъпна както за мобилни устройства, така и за компютри;  

- Да е безопасен и добре защитен;  

- Да е възможна обратна връзка от потребителите; 

 

- Да свързва сайта с други сайтове, свързани с услугите и мисията на SocialHUB; 

- Да осигурява брояч на посещенията. Така услуга предлага Google Analytics. 

6. Дизайна на уебсайта 
-Навигация: 
![image](https://gitlab.com/DenitoWee123/social-hub/-/raw/main/readme.src/structure.png)

HTML:
- Лого на уебсайта:
 <a href="/" class="logo">
          <svg width="162" height="105">
            <circle cx="80" cy="100" r="50" fill="orange"/>
            <text class="neon" x="80" y="100" text-anchor="middle" font-size="36" fill="#ffffff">SocialHub</text>
          </svg>
        </a>

- Навигационен панел:
<nav>
          <form class="search-form">
            <input type="search" placeholder="Search...">
            <button type="submit"><i class="fas fa-search"></i></button>
          </form>
          <ul>
            <li><a href="/">Home</a></li>
            <li><a href="/about">About</a></li>
            <li><a href="/blog">Blog</a></li>
            <li><a href="/contact">Contact</a></li>
          </ul>
        </nav>

- Футър
<footer>
    <div class="container">
      <p>@Copyright 2023 SocialHub</p>
      <ul>
        <li><a href="/">Home</a></li>
        <li><a href="/link1">link1</a></li>
        <li><a href="/link2">link2</a></li>
        <li><a href="/link3">link3</a></li>
      </ul>
    </div>
  </footer>

CSS:
- Фон:
body {
  font-family: Arial, sans-serif;
  background-color: rgb(100, 205, 231);
}

- Хедър:
header {
  background-color: #333;
  color: #fff;
  padding: 20px;
  height: 128px;
}

- Лого:
header .logo {
  display: block;
  color: #fff;
  font-size: 18px;
  text-decoration: none;
}

- Навигационен панел:
header nav {
  float: right;
}

header nav ul {
  list-style: none;
  margin: 0;
  padding: 0;
  background-color: #00b652;
}

header nav ul li {
  display: inline-block;
  margin-left: 10px;
}

header nav ul li a {
  display: block;
  padding: 10px 15px;
  color: #fff;
  text-decoration: none;
}

header nav ul li a:hover {
  text-decoration: underline;
  background-color: #444;
}

header nav .search-form {
  float: right;
  margin-left: 10px;
}

header nav .search-form input {
  width: 150px; /* adjust to desired width */
  height: 30px; /* adjust to desired height */
  padding: 0 10px; /* adjust the padding */
  border: none; /* remove border */
  border-radius: 5px; /* add border-radius */
  font-size: 14px; /* adjust font size */
}

header nav .search-form button {
  width: 40px; /* adjust to desired width */
  height: 38px; /* adjust to desired height */
  background-color: #4d4545; /* adjust the background color */
  color: #fff; /* adjust the text color */
  border: none; /* remove border */
  border-radius: 0px; /* add border-radius */
  font-size: 14px; /* adjust font size */
  cursor: pointer; /* add cursor */
  padding: 0 10px; /* adjust the padding */
}

- Футър:
footer {
  background-color: #333;
  color: #fff;
  padding: 5px;
  text-align: center;
  height: 30px;
}

footer p {
  margin: 0;
  font-size: 14px;
}

footer ul {
  list-style: none;
  margin: 0;
  padding: 0;
}

footer ul li {
  display: inline-block;
  margin-right: 10px;
}

footer ul li a {
  color: #fff;
  text-decoration: none;
}

footer ul li a:hover {
  text-decoration: underline;
}

- Сайдбар:
.sidebar {
  width: 300px;
  background-color: #eee;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
  padding: 20px;
  float:right;
}

.sidebar h2 {
  margin: 0;
  font-size: 18px;
}

.sidebar ul {
  list-style: none;
  margin: 0;
  padding: 0;
}

.sidebar li {
  margin: 10px 0;
}

.sidebar a {
  color: #333;
  text-decoration: none;
}

7. Използвани технологии 

- документация – MS Word; 

- сайт - HTML5 + CSS; 

8. Изисквания към потребителския интерфейс  

Потребителят трябва да има устройство с интернет, което да можа да отвори интернет браузър и да визуализира сайт. 